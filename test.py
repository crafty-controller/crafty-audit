import os
import sys
import webbrowser

from mako.template import Template


def get_file_data(filename):
    file_data = ""

    try:
        with open(filename, 'r') as f:
            file_data = f.readlines()
    except Exception as e:
        print('Unable to read file due to error: {}'.format(e))
        pass

    return file_data


def strip_trash(text):
    text = text.replace("',", "")
    text = text.replace(" '", "")
    text = text.replace("#", "")
    text = text.replace("[]", "")
    text = text.replace("- ", "")
    text = text.replace("  ", "")
    text = text.replace("|-|-|", "")
    text = text.replace("\n", "")
    return text


def parse_data(file_data):
    return_dict = {}
    count = 0
    systemd = []
    services = []
    boot_services = []
    kern_mods = []
    pam = []
    shells = []
    users = []
    packages = []
    suggestions = []
    warnings = []

    for line in file_data:
        parsed = strip_trash(line)
        count += 1
        parts = parsed.split("=")

        if len(parts) > 1:

            # is this a systemd_unit?
            if parts[0][0:17] == "systemd_unit_file":
                # let's split this on the pipe, and join back together with " - "
                temp = parts[1].split("|")
                systemd.append(temp)

            elif parts[0][0:15] == "running_service":
                services.append(parts[1])

            elif parts[0][0:12] == "boot_service":
                boot_services.append(parts[1])

            elif parts[0][0:20] == "loaded_kernel_module":
                kern_mods.append(parts[1])

            elif parts[0][0:10] == "pam_module":
                pam.append(parts[1])

            elif parts[0][0:15] == "available_shell":
                shells.append(parts[1])

            elif parts[0][0:9] == "real_user":
                users.append(parts[1].split(","))

            elif parts[0][0:10] == "suggestion":
                temp = parts[1].split("|")
                suggestions.append(temp)

            elif parts[0][0:7] == "warning":
                temp = parts[1].split("|")
                warnings.append(temp)

            else:
                # add item to our dictionary
                return_dict[parts[0]] = parts[1]

            packages = return_dict.get('installed_packages_array', "|").split("|")

            return_dict['packages'] = packages
            return_dict['users'] = users
            return_dict['boot'] = boot_services
            return_dict['kern'] = kern_mods
            return_dict['pam'] = pam
            return_dict['shells'] = shells
            return_dict['systemd_units'] = systemd
            return_dict['services'] = services
            return_dict['suggestions'] = suggestions
            return_dict['warnings'] = warnings


    return return_dict


data = get_file_data("lynis-report.dat")

keys = parse_data(data)

t = Template(filename='web/template.html')
html_data = t.render(data=keys)

html_report = os.path.join("web", "report.html")

with open(html_report, 'w') as f:
    f.write(html_data)

webbrowser.open("file://{}".format(os.path.abspath(html_report)))
